$(document).ready(function () {
   var produtos_iniciais = getProdutos();//pega os usuarios da primeira pagina para preencher a tabela
    
});
    

function getProdutos() {
    var produtos = Array();
     produtos[0] = ["NOME", "PREÇO DE CUSTO (R$)", "PREÇO FINAL (R$)", "QUANTIDADE", "DATA DE COMPRA"];//cABEÇALHO
    var i = 1
    firebase.database().ref(`produtos`).once('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var Data = childSnapshot.val();
        produtos[i] = [Data['name'],  Data['precoCusto'], Data['preco'], Data['qtd'], Data['dataEntrada']]
        i++
      });
        var produtosTable = criarTabela($("#produtos"), produtos);
    });
    
    return produtos;

}

function criarTabela(container, data) {
    var table = $('<table class = "table table-hover table-striped table-bordered">');
    var rowHead = $("<thead/>");
    for(var i = 0; i < data[0].length; i++){
        rowHead.append($("<th/>").text(data[0][i]));
    }
    table.append(rowHead);
    
    var rowBody = $("<tbody/>");
    for(var i = 1; i < data.length; i++){
        var row = $("<tr/>");
        for(var j = 0; j < data[i].length; j++){
            row.append($("<td/>").text(data[i][j]));
        }
        rowBody.append(row);
    }
    table.append(rowBody);
    return container.append(table);
}
