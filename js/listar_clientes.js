$(document).ready(function () {
   var users_iniciais = getUsers();//pega os usuarios da primeira pagina para preencher a tabela
    
});
    

function getUsers() {
    var users = Array();
     users[0] = ["NAME", "CPF", "EMAIL", "ENDERECO", "NASCIMENTO", "CELULAR", "TELEFONE"];//cABEÇALHO
    var i = 1
    firebase.database().ref(`clientes`).once('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var Data = childSnapshot.val();
        users[i] = [Data['name'],  Data['cpf'], Data['email'], Data['endereco'], Data['nascimento'], Data['celular'], Data['telefone']]
        i++
      });
        var userTable = criarTabela($("#tabela"), users);
    });
    
    return users;

}

function criarTabela(container, data) {
    var table = $('<table class = "table table-hover table-striped table-bordered">');
    var rowHead = $("<thead/>");
    for(var i = 0; i < data[0].length; i++){
        rowHead.append($("<th/>").text(data[0][i]));
    }
    table.append(rowHead);
    
    var rowBody = $("<tbody/>");
    for(var i = 1; i < data.length; i++){
        var row = $("<tr/>");
        for(var j = 0; j < data[i].length; j++){
            row.append($("<td/>").text(data[i][j]));
        }
        rowBody.append(row);
    }
    table.append(rowBody);
    return container.append(table);
}
