$(document).ready(function () {
   var vendas_iniciais = getVendas();
    
});
    

function getVendas() {
    var vendas = Array();
     vendas[0] = ["CPF DA NOTA", "NOME COMPRADOR", "DATA DA COMPRA", "VALOR (R$)"];//cABEÇALHO
    var i = 1
    firebase.database().ref(`vendas`).once('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var Data = childSnapshot.val();
        vendas[i] = [Data['cpfComprador'],  Data['nomeComprador'], Data['dataCompra'], Data['valor']]
        i++
      });
        var vendaTable = criarTabela($("#venda"), vendas);
    });
    
    return vendas;

}

function criarTabela(container, data) {
    var table = $('<table class = "table table-hover table-striped table-bordered">');
    var rowHead = $("<thead/>");
    for(var i = 0; i < data[0].length; i++){
        rowHead.append($("<th/>").text(data[0][i]));
    }
    table.append(rowHead);
    
    var rowBody = $("<tbody/>");
    for(var i = 1; i < data.length; i++){
        var row = $("<tr/>");
        for(var j = 0; j < data[i].length; j++){
            row.append($("<td/>").text(data[i][j]));
        }
        rowBody.append(row);
    }
    table.append(rowBody);
    return container.append(table);
}
