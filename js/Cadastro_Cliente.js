var nameInput = document.getElementById('nameInput');
var emailInput = document.getElementById('emailInput');
var facebookInput = document.getElementById('facebookInput');
var instagramInput = document.getElementById('instagramInput');
var enderecoInput = document.getElementById('enderecoInput');
var nascimentoInput = document.getElementById('nascimentoInput');
var cpfInput = document.getElementById('cpfInput');
var telefoneInput = document.getElementById('telefoneInput');
var celularInput = document.getElementById('celularInput');
var addButton = document.getElementById('cadUser');


// Ao clicar no botão
addButton.addEventListener('click', function () {
create(nameInput.value, emailInput.value, facebookInput.value, instagramInput.value, enderecoInput.value, nascimentoInput.value, cpfInput.value, telefoneInput.value, celularInput.value);
  
});

// Ao clicar no botão deslogar
DeslogarLink.addEventListener('click', function () {
   firebase.auth().signOut().then(function() {
    window.location.href="login.html"
}).catch(function(error) {
  // An error happened.
});
});

// Função para criar um registro no Firebase
function create(nome, email, facebook, instagram, endereco, nascimento, cpf, telefone, celular) {
    var email_64 = btoa(email)
    firebase.database().ref('clientes/' + email_64).set({
        name: nome,
        email:email_64,
        facebook: facebook,
        instagram: instagram,
        endereco: endereco,
        nascimento: nascimento,
        cpf:cpf,
        telefone:telefone,
        celular:celular
        });
}